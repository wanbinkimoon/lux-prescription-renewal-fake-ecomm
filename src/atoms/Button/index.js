import React, {useMemo} from 'react';
import styled from 'styled-components';
import {Button as MButton} from '@material-ui/core';


const Button = ({
  children,
  underlined,
  inverted,
  grey,
  ...props
}) => {
  const color = useMemo(() => {
    if (grey) {
      return 'default';
    }
    if (inverted) {
      return 'secondary';
    }

    return 'primary';
  }, [grey, inverted]);

  if (props.variant === 'text') {
    return (
      <TextButton {...props} disableFocusRipple={true} disableRipple={true}>
        <Label underlined={underlined}>{children}</Label>
      </TextButton>
    );
  }

  return (
    <MyButton color={color} variant="contained" {...props} disableElevation>
      <Label underlined={underlined}>{children}</Label>
    </MyButton>
  );
};

export default Button;

const MyButton = styled(MButton)`
  text-transform: none;
  border-radius: 22px;
  padding: 8px 28px;
  font-size: 16px;
  font-weight: 900;
`;

const TextButton = styled(MButton)`
  &:hover {
    background-color: transparent;
  }
`;

const Label = styled.span`
  border-bottom: ${props =>
    props.underlined ? 'solid 1px currentcolor' : 'none'};
`;
