import React from 'react';
import Slider from '@material-ui/core/Slider';
import {withStyles} from '@material-ui/core/styles';
import styled from 'styled-components';

import blueCode from '../../assets/icons/blue-code.svg';

const CustomThumb = (props) => {
  return (
    <Thumb {...props}>
      <Arrows />
    </Thumb>
  );
};

const Range = props => {
  return (
    <CustomSlider
      min={0.5}
      max={1.5}
      step={0.001}
      value={props.value}
      onChange={props.onChange}
      defaultValue={1}
      ThumbComponent={CustomThumb}
    />
  );
};

export default Range;

const Thumb = styled.div`
  background-color: ${props => props.theme.colors.white};
  border: 2px solid ${props => props.theme.colors.primary.first};
  height: 48px;
  width: 48px;
  margin: -20px 0 0 -24px;
  box-shadow: 'inherit';
`;

const Arrows = styled.img.attrs(() => ({
  src: blueCode,
}))``;

const CustomSlider = withStyles({
  root: {
    color: 'primary',
    height: 8,
    width: 392,
  },
  thumb: {
    '&:focus, &:hover, &$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  track: {
    height: 10,
    borderRadius: 5,
  },
  rail: {
    height: 10,
    borderRadius: 5,
    color: '#ccd1d6',
  },
})(Slider);
