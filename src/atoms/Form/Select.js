import React from 'react';
import {useTranslation} from 'react-i18next';
import styled from 'styled-components';
import {
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select as MSelect,
} from '@material-ui/core';

import chevronDown from '../../assets/icons/chevron-down.svg';

const Select = ({
  label,
  children,
  options,
  errorKey,
  ...props
}) => {
  const {t} = useTranslation();
  const {error, name} = props;

  const handleChange = (e) => {
    props.onChange && props.onChange(e);
  };

  return (
    <StyledFormControl variant="outlined" {...props}>
      <ColorLabel id={`${name}-label`} className={error ? 'error' : ''}>
        {label}
      </ColorLabel>
      <StyledSelect
        value={props.value}
        onChange={handleChange}
        labelId={`${name}-label`}
        id={name}
        name={name}
        error={error}
        // onBlur={field.onBlur}
        label={label}
        MenuProps={{
          anchorOrigin: {vertical: 'center', horizontal: 'center'},
          transformOrigin: {
            vertical: -64,
            horizontal: 'center',
          },
        }}
        IconComponent={ChevronIcon}
        displayEmpty
      >
        {options.map(({value, label}, i) => (
          <Item key={value} value={value} last={i === options.length - 1}>
            {t(label)}
          </Item>
        ))}
      </StyledSelect>
      {error && <FormHelperText error={true}>{t('error')}</FormHelperText>}
    </StyledFormControl>
  );
};
export default Select;

const StyledFormControl = styled(FormControl)`
  margin-bottom: 24px;

  .MuiFormLabel-root::first-letter {
    text-transform: uppercase;
  }
`;

const StyledSelect = styled(MSelect)`
  width: 392px; /* TODO: example width, use percentage */
  width: 100%;
  padding-right: 10px;
  .MuiSelect-iconOutlined {
    right: 14px;
  }
`;

const ColorLabel = styled(InputLabel)``;

const Item = styled(MenuItem)`
  height: 56px;
  border-bottom: 2px solid
    ${props => (props.last ? 'transparent' : props.theme.colors.ironGray)};
`;

const ChevronIcon = styled.img.attrs(() => ({
  src: chevronDown,
}))``;
