import {createMuiTheme} from '@material-ui/core/styles';

import * as commonTheme from './common';

export const GlobalStyle = commonTheme.GlobalStyle;

export const styledTheme = {
  colors: commonTheme.colors,
  breakpoints: commonTheme.breakpoints,
};

export const muiTheme = createMuiTheme(commonTheme.muiTheme);