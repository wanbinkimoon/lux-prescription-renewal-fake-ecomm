import React from 'react';
import {ThemeProvider} from 'styled-components';
import {
  StylesProvider,
  ThemeProvider as MaterialThemeProvider,
} from '@material-ui/core';

import MainNavigation from './MainNavigation';
import {styledTheme, muiTheme} from './theme';

export default class App extends React.Component {
  state = {
    loading: true,
    theme: null,
  };

  async componentDidMount() {
    // Async init stuffs
    this.setState({
      loading: false,
    });
  }

  render() {
    if (this.state.loading) {
      return <h1>Loading...</h1>;
    }

    return (
      <ThemeProvider theme={styledTheme}>
        <MaterialThemeProvider theme={muiTheme}>
          <StylesProvider injectFirst>
            <MainNavigation />
          </StylesProvider>
        </MaterialThemeProvider>
      </ThemeProvider>
    );
  }
}
