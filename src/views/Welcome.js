import React, { useState } from 'react';
import styled from 'styled-components';
import Form from './Form';
import PrescriptionStatus from './PrescriptionStatus';
import Input from '../atoms/Form/Input';
import Button from '../atoms/Button';

function makeid(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export default function Welcome() {
  const [consultationID, setConsultationID] = useState(makeid(24));
  const [storeID, setStoreID] = useState('lens-crafters');
  const [productType, setProductType] = useState('CL');
  const [redirectUrl, setRedirectUrl] = useState('https://google.com');

  const customURL = `${process.env.REACT_APP_WEBAPP_URL}?storeID=${storeID}&consultationID=${consultationID}&productType=${productType}&redirectUrl=${redirectUrl}`;
  return (
    <Layout>
      <h1>Welcome to the fake ecommerce page</h1>
      <hr />
      <nav>
        <Button>
          <a target="_blank" href={`${process.env.REACT_APP_WEBAPP_URL}`}>
            Exam - User full path
          </a>
        </Button>
        <hr />
        <Button>
          <a target="_blank" rel="noopener noreferrer" href={customURL}>
            Exam - User data entry
          </a>
        </Button>
        <code>
          <b>custom URL: </b>
          <span>{customURL}</span>
        </code>
        <div>
          <StyledInput
            label="Consultation ID"
            value={consultationID}
            onChange={(e) => setConsultationID(e.target.value)}
          />
          <StyledInput label="Store ID" value={storeID} onChange={(e) => setStoreID(e.target.value)} />
          <StyledInput label="Product Type" value={productType} onChange={(e) => setProductType(e.target.value)} />
          <StyledInput label="Redirect URL" value={redirectUrl} onChange={(e) => setProductType(e.target.value)} />
        </div>
      </nav>
      <hr />
      <h2>User data form</h2>
      <Form consultationID={consultationID} />
      <hr />
      <h2>Get prescription status</h2>
      <PrescriptionStatus consultationID={consultationID} />
    </Layout>
  );
}

const Layout = styled.main`
  display: flex;
  align-items: center;
  flex-direction: column;
  margin: 32px;

  hr {
    width: 100%;
    margin: 32px 0;
  }

  a,
  a:visited,
  a:hover,
  a:active,
  a:focus {
    color: white;
    text-decoration: none;
  }

  nav {
    background-color: ${({ theme }) => theme.colors.gray100};
    width: 100%;
    padding: 32px 0;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    display: flex;
    & > * {
      margin: 0 16px;
    }

    hr {
      width: 100%;
      margin: 32px 0;
    }
  }

  code {
    margin: 16px 0;
  }
`;

const StyledInput = styled(Input)`
  background-color: ${({ theme }) => theme.colors.white};
  margin: 16px;
  & > div {
    & > input {
      color: ${({ theme }) => theme.colors.black};
      cursor: pointer;
    }
  }
`;
