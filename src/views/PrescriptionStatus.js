import React, {useState} from 'react';
import JSONTree from 'react-json-tree';
import Button from '../atoms/Button';

export default function PrescriptionStatus(props) {
  const [prescriptionData, setPrescriptionData] = useState({})
  
  const fetchPrescriptionStatus = async () => {
    const requestOptions = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow',
    };


    await fetch(`${process.env.REACT_APP_API_ENDPOINT}/api/exam/status/${props.consultationID}`, requestOptions)
      .then((response) => response.text())
      .then((response) => {
        setPrescriptionData(JSON.parse(response)._doc)
        return response
      })
      .then((result) => console.log(result))
      .catch((error) => console.log('error', error));
  }
  console.log(prescriptionData)
  return (
    <div>
      <Button onClick={fetchPrescriptionStatus}>Get prescription status</Button>
      <main>
        <JSONTree data={prescriptionData} /> 
      </main>
    </div>
  )
}