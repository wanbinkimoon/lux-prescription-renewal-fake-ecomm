import React, { useState } from 'react';
import styled from 'styled-components';
import Input from '../atoms/Form/Input';
import Button from '../atoms/Button';

export default function Form(props) {
  const [firstName, setFirstName] = useState('Giovanna');
  const [lastName, setLastName] = useState('Cascio');
  const [email, setEmail] = useState(`${firstName}.${lastName}@gmail.com`);
  const [phoneNumber, setPhone] = useState(`123456789`);
  const [usState, setUsState] = useState(`california`);
  const [gender, setGender] = useState(`F`);
  const [age, setAge] = useState(`23`);
  const [ssn, setSSN] = useState(`1234`);

  console.log(process.env.REACT_APP_API_ENDPOINT);

  const sendUserData = async () => {
    const body = JSON.stringify({
      firstName: firstName,
      lastName: lastName,
      state: usState,
      age: age,
      gender: gender,
      ssn: ssn,
      email: email,
      phoneNumber: phoneNumber,
    });

    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: body,
      redirect: 'follow',
    };

    await fetch(`${process.env.REACT_APP_API_ENDPOINT}/api/exam/save/user/${props.consultationID}`, requestOptions)
      .then((response) => response.text())
      .then((result) => console.log(result))
      .catch((error) => console.log('error', error));
  };

  return (
    <Layout>
      <div>
        <StyledInput label="Costumer name" value={firstName} onChange={(e) => setFirstName(e.target.value)} />
        <StyledInput label="Costumer lastName" value={lastName} onChange={(e) => setLastName(e.target.value)} />
      </div>
      <div>
        <StyledInput label="Costumer email" value={email} onChange={(e) => setEmail(e.target.value)} />
        <StyledInput label="Costumer phone" value={phoneNumber} onChange={(e) => setPhone(e.target.value)} />
      </div>
      <div>
        <StyledInput label="Costumer gender" value={gender} onChange={(e) => setGender(e.target.value)} />
        <StyledInput label="Costumer age" value={age} onChange={(e) => setAge(e.target.value)} />
      </div>
      <div>
        <StyledInput label="Costumer state" value={usState} onChange={(e) => setUsState(e.target.value)} />
        <StyledInput label="Costumer ssn" value={ssn} onChange={(e) => setSSN(e.target.value)} />
      </div>
      <Button onClick={sendUserData}>
        Send to Used Data
      </Button>
    </Layout>
  );
}

const Layout = styled.form`
  background-color: ${({ theme }) => theme.colors.gray100};
  width: 100%;
  padding: 32px 0;
  align-items: center;
  justify-content: center;
  display: flex;
  flex-direction: column;

  & > * {
    margin: 0 16px;
  }
`;

const StyledInput = styled(Input)`
  background-color: ${({ theme }) => theme.colors.white};
  margin: 16px;
  & > div {
    & > input {
      color: ${({ theme }) => theme.colors.black};
      cursor: pointer;
    }
  }
`;
